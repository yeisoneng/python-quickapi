.. QuickAPI documentation master file, created by
   sphinx-quickstart on Wed Jul 20 15:28:58 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Python QuickAPI
===============
Easy python access to REST APIs.


Installation
------------
.. code-block:: python

    pip install quickapi



Authentication
--------------
As in `requests module <http://docs.python-requests.org/en/master/user/authentication/>`_.

Basic Authentication
^^^^^^^^^^^^^^^^^^^^
.. code-block:: python

    from requests.auth import HTTPBasicAuth

    url = 'http://restframework.herokuapp.com/'
    auth = HTTPBasicAuth('admin', 'admin')
    #auth = ('admin', 'admin')
    api = NewAPI(url=url, auth=auth)


Digest Authentication
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    from requests.auth import HTTPDigestAuth

    url = 'http://restframework.herokuapp.com/'
    auth = HTTPDigestAuth('admin', 'admin')
    api = NewAPI(url=url, auth=auth)


OAuth 1 Authentication
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    from requests_oauthlib import OAuth1

    url = 'http://restframework.herokuapp.com/'
    auth = OAuth1('YOUR_APP_KEY', 'YOUR_APP_SECRET',
                  'USER_OAUTH_TOKEN', 'USER_OAUTH_TOKEN_SECRET')
    api = NewAPI(url=url, auth=auth)


JWT Authentication
^^^^^^^^^^^^^^^^^^

.. code-block:: python

    from requests_oauthlib import OAuth1

    url = 'http://restframework.herokuapp.com/'
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJsb2dnZWRJbkFzIjoiYWRtaW4iLCJpYXQiOjE0MjI3Nzk2Mzh9.gzSraSYS8EXBxLN_oWnFSRgCzcmJmMjLiuyu5CSpyHI'
    api = NewAPI(url=url, token=token)


Example
-------

.. code-block:: python

    #quickapi_example.py

    from quickapi import NewAPI
    import webbrowser

    url = 'http://restframework.herokuapp.com/'
    auth = ('admin', 'admin')

    #create API y get their endpoints
    api = NewAPI(url=url, auth=auth)
    print(api.endpoints())

    #read current file as text
    file = open(__file__, 'r')
    lines = "".join(file.readlines())
    file.close()

    #get options for API
    response = api.options()
    print(response)

    #get options for endpoint 'snippets'
    response = api.options_snippets()
    print(response)

    #post call
    response = api.post_snippets(title='quickapi_example.py', code=lines, linenos=False, language='python', style='friendly')
    print(response)

    webbrowser.open_new_tab(response['url'])
    webbrowser.open_new_tab(response['highlight'])


